import re
import csv,yaml
import datetime, calendar
from collections import OrderedDict
#OrderedDict((word, True) for word in words)

regex = re.compile('[^0-9]')
class Lectionary:
    _year = 2012 #leap year by default
    _lect = {}
    def __init__(self,lectionary):
        self._lect = lectionary

    def getReading(self,month,day):
        d = datetime.date(2012,month,day)
        return self._lect[d]

        
        
def stripSpace(array):
    regex = re.compile('[^0-9]')
    result = re.sub('[^0-9]','', array[1])
    array[1] = result
    for y in range(len(array)):
        array[y] = array[y].lower()
    return array

    pass
        
if __name__ == "__main__":
    lectionary = {}
    with open('../config/lectionary.csv', newline='') as csvfile:
        spamreader = csv.reader(csvfile, delimiter=',', quotechar='"')

        
        count = 0 
        for row in spamreader:
            array = stripSpace(row)
            del(array[3])
            print(', '.join(array))
            d = datetime.date(2012,int(array[0]),int(array[1]))
            mp = {'ot':array[2],'nt':array[3]}
            ep = {'ot':array[4],'nt':array[5]}
            item = {'mp':mp,'ep':ep}
            lectionary[d]=item
            count +=1
        print(lectionary)

        lect = Lectionary(lectionary)
        today = datetime.datetime.today()
        month = today.month
        day = today.day
        print(lect.getReading(month,day))
        

        
        with open('../config/lectionary.yml', 'w') as outfile:
            yaml.dump(lectionary, outfile, default_flow_style=False)
